enum CitySize {
  Town,       // approximate residents: 1_000
  City,       // approximate residents: 10_000
  Metropolis, // approximate residents: 1_000_000
  Area { residents: u64 }
}

struct City {
  description: String,
  residents: u64,
  is_coastal: bool,
}

impl City {
  fn new(city_size: CitySize, is_coastal: bool) -> Self {
    let city_type = match city_size {
      CitySize::Town => { "town" }
      CitySize::City => { "city" }
      CitySize::Metropolis => { "metropolis" }
      _ => { "unknown-size" }
    };

    let residents: u64 = match city_size {
      CitySize::Town => { 1_000 }
      CitySize::City => { 10_000 }
      CitySize::Metropolis => { 1_000_000 }
      CitySize::Area { residents } => { residents }
    };

    City {
      description: format!("an *{}* of approximately {} residents", city_type, residents),
      residents,
      is_coastal,
    }
  }
}

fn main() {
  let rustville = City::new(CitySize::Area { residents: 10_000_000 }, true);
  println!("This city is {}", rustville.description);

  if rustville.residents > 100_000 {
      println!("Wow!");
  }
}

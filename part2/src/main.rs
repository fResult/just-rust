struct City {
  description: String,
  residents: u64,
  is_coastal: bool
}

fn new_city(residents: u64, is_coastal: bool) -> City {
  City {
    description: format!("a *{}coastal* city of approximately {} residents", if !is_coastal { "non-" } else { "" }, residents),
    residents,
    is_coastal
  }
}

fn main() {
  let rustville: City = new_city(8_500_000, true);

  println!("This city can be described as: {}", rustville.description);

  if rustville.is_coastal {
    println!("It is a coastal city.");
  } else {
    println!("It is not a coastal city.");
  }
}
